package com.example.produto.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.produto.models.Produto;
import com.example.produto.services.ProdutoService;
import com.example.produto.viewobject.ProdutoCliente;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	ProdutoService produtoService;
	
	@PostMapping
	public void inserirProduto(@RequestBody Produto produto) {
		
		produtoService.inserirProduto(produto);
		
		
	}
	
	@GetMapping
	public Iterable<Produto> consultarProtudo(){
		
		return produtoService.consultarTodosProdutos();
		
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity buscarPorID(@PathVariable int id) {
		
		Optional<Produto> optionalProduto = produtoService.consultarPorId(id);
		
		if (optionalProduto.isPresent()) {
			return ResponseEntity.ok(optionalProduto.get());
		}
		
		return ResponseEntity.notFound().build();
		
	}
	
	@GetMapping("/nome/{nome}")
	public List<Produto> buscarPorNome(@PathVariable String nome) {
		
		return produtoService.consultarNome(nome);
		
	}
	
	@PostMapping("/produtocliente")
	public ProdutoCliente buscarProdutoCliente(@RequestBody ProdutoCliente produtoCliente) {
		
		return produtoService.consultarProdutoCliente(produtoCliente);
		
	}
}
