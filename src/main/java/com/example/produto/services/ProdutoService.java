package com.example.produto.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.produto.models.Produto;
import com.example.produto.repositories.ProdutoRepository;
import com.example.produto.viewobject.Cliente;
import com.example.produto.viewobject.ProdutoCliente;

@Service
public class ProdutoService {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	RestTemplate restTemplate = new RestTemplate();
	
	public void inserirProduto(Produto produto) {
		
		produtoRepository.save(produto);
		
		
	}
	
	public Iterable<Produto> consultarTodosProdutos(){
		
		return produtoRepository.findAll();
		
		
	}
	
	public Optional<Produto> consultarPorId(int id) {
		
		return produtoRepository.findById(id);
	}
	
	public List<Produto> consultarNome(String nome) {
		
		return produtoRepository.findAllByNome(nome);
	}

	public ProdutoCliente consultarProdutoCliente(ProdutoCliente produtoCliente) {
		Optional<Produto> produto = consultarPorId(produtoCliente.getIdProduto());
		Cliente cliente = consultarCliente(produtoCliente.getIdCliente());
		produtoCliente.setNomeProduto(produto.get().getNome());
		System.out.println(cliente.getNomeCliente());
		produtoCliente.setNomeCliente(cliente.getNomeCliente());
		return produtoCliente;
		
	}
	
	private Cliente consultarCliente(int idCliente) {
		return restTemplate.getForObject("http://localhost:8082/cliente/" + idCliente, Cliente.class);
	}
}
