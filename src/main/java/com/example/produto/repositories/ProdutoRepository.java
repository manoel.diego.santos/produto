package com.example.produto.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.produto.models.Produto;



public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

	public List<Produto> findAllByNome(String nome);

}
